package com.mundojogo.info.app.holdem.presentation.screens.info

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.Globe.Titans.largest.population.app.domain.usecases.GetCountriesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GTPlayersScreenViewModel @Inject constructor(
    private val getCountriesUseCase: GetCountriesUseCase
): ViewModel() {
    val t = getCountriesUseCase.data

    fun getNext(){
        viewModelScope.launch {
            getCountriesUseCase.getNext()
        }
    }

    fun getLast(){
        viewModelScope.launch {
            getCountriesUseCase.getLast()
        }
    }

}