package com.Globe.Titans.largest.population.app.data.storage.implementation

import com.Globe.Titans.largest.population.app.R
import com.Globe.Titans.largest.population.app.data.country.CountryModel
import com.Globe.Titans.largest.population.app.data.storage.CountryStorageRepository

class CountriesExternalStorage: CountryStorageRepository {

    private var countryIterator = 0

    private val mainList = listOf(
        CountryModel(
            title = "China - 1,407,839,000 people.",
            countryImage = R.drawable.country1,
            description = "The People's Republic of China ranks first in terms of population. More than 1.4 billion people live on its lands located in East Asia. The largest megacities in the world are also located here: Chongqing (about 30 million inhabitants), Shanghai and Beijing (more than 20 million)." +
                    "The age of Chinese civilization is about 5 thousand years, during which the ruling dynasties replaced each other, leaving behind a rich cultural heritage and architectural landmarks that have survived to this day."
        ),
        CountryModel(
            title = "India - 1,383,385,000 people.",
            countryImage = R.drawable.country2,
            description = "Along with China, India surprises with the number of residents living in it - 1.38 billion. Visiting for the first time in Delhi, Mumbai, Calcutta and other cities with a population of over a million people are shocked by the crowded streets, widespread crowding, lack of compliance with road safety rules and unsanitary conditions. At the same time, the imagination of travelers is struck by the neighborhood of dilapidated city slums with modern skyscrapers and ancient architectural masterpieces."
        ),
        CountryModel(
            title = "USA - 329,957,441 people.",
            countryImage = R.drawable.country3,
            description = "The United States of America, with 329 million people living in it, closes the top three largest countries in terms of population. At the same time, the state is the fourth in area; its states occupy a significant part of the mainland of North America, as well as several dozen islands (Hawaiian and adjacent to Alaska)." +
                    "As one of the most influential powers, America makes a huge contribution to world culture, science and international relations."
        ),
        CountryModel(
            title = "Indonesia - 266,911,900 people.",
            countryImage = R.drawable.country4,
            description = "The country of Southeast Asia, located on both sides of the equator, is one of the most densely populated. According to 2019 data, the number of people living in it was almost 267 million. In addition, the republic is one of the largest island states, as it occupies a significant part of the Malay archipelago and New Guinea." +
                    "Original culture, favorable climate, untouched tropical forests, unique flora and fauna, as well as the ideal coasts of the Pacific and Indian Oceans make Indonesia extremely attractive for ecotourism lovers."
        ),
        CountryModel(
            title = "Pakistan - 220,065,996 people.",
            countryImage = R.drawable.country5,
            description = "Located in northern South Asia, Pakistan was formerly part of the British India colony. It was only in the middle of the 20th century that it gained its independence, becoming an independent state. To date, the number of residents of the republic is 220 million, most of whom (about 173 million) are Muslims. In terms of the number of representatives of this religion, Pakistan is second only to Indonesia (more than two hundred million)."
        ),
        CountryModel(
            title = "Brazil - 212,572,768 people.",
            countryImage = R.drawable.country6,
            description = "The state located on the territory of the South American continent is the birthplace of more than 212 million people. Despite the large number, there is enough space for everyone - the country ranks 5th in the world in terms of area (8.5 million square kilometers)." +
                    "Brazil is extremely attractive to tourists: clean golden beaches, rainy Amazon jungle, picturesque Iguazu waterfalls and other natural attractions attract more than 5 million travelers every year."
        ),
        CountryModel(
            title = "Nigeria - 210,615,916 people.",
            countryImage = R.drawable.country7,
            description = "Nigeria is the most populous country in Africa with a population of 210 million. At the same time, the area occupied by the country is not so large - about 923 thousand square kilometers. Most of it belongs to savannahs and tropical forests; in the latter you can find trees up to 45 m high." +
                    "Despite the fact that the basis of the country's economy is oil production, it is one of the poorest. More than half of people are begging; due to illiteracy and difficulties of employment, they have to earn bread by begging."
        ),
        CountryModel(
            title = "Bangladesh - 171,987,524 people.",
            countryImage = R.drawable.country8,
            description = "According to the latest data, the number of people living in Bangladesh is almost 172 million. Residents of the state, which occupies the 8th place in terms of population in the world, are forced to hude on the territory of only 148 thousand square kilometers (the country is on the 95th line in terms of its area)." +
                    "The status of an independent republic was obtained by Bangladesh relatively recently - in 1988. Previously, the lands were under the rule of the British colonizers. In this regard, the South Asian"
        ),
        CountryModel(
            title = "Russia - 146,748,590 people.",
            countryImage = R.drawable.country9,
            description = "The Russian Federation, which is home to almost 147 million people, is the largest state in the world and occupies an eighth of the land. Its area exceeds 17 million square kilometers. Thanks to its extensive lands, the country boasts a variety of natural zones (Arctic deserts, tundra, taiga, steppes, etc.)." +
                    "Travelers in Russia may be surprised by the presence of 11 time zones on the territory of the state: the largest time discrepancy with Moscow is 9 hours."
        ),
        CountryModel(
            title = "Mexico - 127,792,286 people.",
            countryImage = R.drawable.country10,
            description = "The population of Mexico is almost 128 million people, with 9 million living in the capital and 22 within the agglomeration. Mexicans prefer to live in megacities, but in remote places from cities there are still remote villages with no signs of civilization.\n" +
                    "The country where Spanish, Caribbean and Indian cultures are intertwined seems unusually colorful. Specific cuisine and special holidays (for example, the Day of the Dead) remain in the memory of tourists for a long time. "
        ),
        CountryModel(
            title = "Japan - 125,950,000 people",
            countryImage = R.drawable.country11,
            description = "The name of the state is translated from Japanese as \"the place where the sun rises\", so often poets in their works call Japan the Land of the Rising Sun." +
                    "Japan, with a population of almost 126 million people, has a fairly developed economy, ranking 3rd in the world in terms of GDP. Due to favorable living conditions, its average duration is 85 years, and the child mortality rate is close to the minimum."
        ),
        CountryModel(
            title = "Ethiopia - 112,079,000 people.",
            countryImage = R.drawable.country12,
            description = "The East African state with access to the Red Sea covers an area of 1,100 thousand square kilometers. It is the second largest country on the continent after Nigeria." +
                    "The settlement of the Ethiopian Highlands began about 3 million years ago, as evidenced by the remains of Australopithecus found. The warm climate and generous nature attracted people to these lands: throughout history, Ethiopia has remained densely populated. Currently, 112 million people live here."
        ),
        CountryModel(
            title = "Philippines - 109,603,270 people.",
            countryImage = R.drawable.country13,
            description = "The Republic, which occupies more than 7,000 islands of the Malay archipelago, is inhabited by 109 million people, more than half of whom live on the island. Luzon. A tenth of the people of the Philippines are abroad almost all year round, where they go to make money (mainly doing households in private homes). At home, Filipinos work on coconut, rice and banana farms, as well as in the service sector."
        ),
        CountryModel(
            title = "Egypt - 101,502,040 people.",
            countryImage = R.drawable.country14,
            description = "The state in North Africa with its capital on the Sinai Peninsula of Asia has 101 million citizens." +
                    "Egypt beckons travelers with the opportunity to see the evidence of an ancient empire that existed for several millennia BC; among them are pyramids, the Sphinx, the temple of Horus, the Valley of the Kings and other historical objects."
        ),
        CountryModel(
            title = "Vietnam - 96,208,984 people.",
            countryImage = R.drawable.country15,
            description = "The state with a population of 96 million people occupies the eastern lands of Indochina and has access to the South China Sea. Divided between America and France during World War II, the country fought for its sovereignty for many years, until 1975. After the end of the war, the northern and southern territories were united to create the Socialist Republic of Vietnam."
        ),
    )

    override fun iteratorMinus() {
        val result = countryIterator-1
        if (result<0) return
        countryIterator = result
    }

    override fun iteratorPlus() {
        val result = countryIterator+1
        if (result>=mainList.size) return
        countryIterator = result
    }

    override fun getCountry(): CountryModel {
        return mainList[countryIterator]
    }
}