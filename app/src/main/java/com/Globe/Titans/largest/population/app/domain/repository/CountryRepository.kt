package com.Globe.Titans.largest.population.app.domain.repository

import com.Globe.Titans.largest.population.app.data.country.CountryModel

interface CountryRepository {
    fun getFirstCountry(): CountryModel
    fun getNextCountry(): CountryModel
    fun getLastCountry(): CountryModel
}