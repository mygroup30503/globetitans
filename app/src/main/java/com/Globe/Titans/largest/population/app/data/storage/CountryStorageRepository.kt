package com.Globe.Titans.largest.population.app.data.storage

import com.Globe.Titans.largest.population.app.data.country.CountryModel

interface CountryStorageRepository {
    fun iteratorMinus()
    fun iteratorPlus()
    fun getCountry(): CountryModel
}