package com.Globe.Titans.largest.population.app.presentation.screens.waiting

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.Globe.Titans.largest.population.app.presentation.utils.Fonts
import com.Globe.Titans.largest.population.app.presentation.utils.GlobeTitansScreens
import kotlinx.coroutines.delay

@Composable
fun GTLoadingScreen(navHostController: NavHostController) {
    LaunchedEffect(key1 = true) {
        delay((0..2000L).random())
        navHostController.navigate(GlobeTitansScreens.GT_HOME) {
            popUpTo(GlobeTitansScreens.GT_LOADING) { inclusive = true }
        }
    }
    Box(
        modifier = Modifier
            .padding(horizontal = 32.dp)
            .fillMaxSize(), contentAlignment = Alignment.BottomCenter
    ) {
        Text(
            text = "Loading",
            fontSize = 40.sp,
            fontWeight = FontWeight.Bold,
            fontFamily = Fonts.font,
            modifier = Modifier.padding(10.dp)
        )
    }
}