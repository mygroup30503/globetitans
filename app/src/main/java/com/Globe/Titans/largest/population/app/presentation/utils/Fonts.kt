package com.Globe.Titans.largest.population.app.presentation.utils

import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import com.Globe.Titans.largest.population.app.R


object Fonts {
    val font = FontFamily(Font(R.font.font))
}