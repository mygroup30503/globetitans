package com.Globe.Titans.largest.population.app.domain.usecases



import com.Globe.Titans.largest.population.app.data.country.CountryModel
import com.Globe.Titans.largest.population.app.domain.repository.CountryRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

class GetCountriesUseCase @Inject constructor(
    private val repository: CountryRepository
) {
    private val _data = MutableStateFlow(repository.getFirstCountry())
    val data: StateFlow<CountryModel> = _data

    suspend fun getNext(){
        repository.getNextCountry().let { _data.emit(it) }
    }

    suspend fun getLast(){
        repository.getLastCountry().let { _data.emit(it) }
    }
}