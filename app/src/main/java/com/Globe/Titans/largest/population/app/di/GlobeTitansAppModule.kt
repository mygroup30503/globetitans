package com.Globe.Titans.largest.population.app.di

import com.Globe.Titans.largest.population.app.data.repository.CountryRepositoryImpl
import com.Globe.Titans.largest.population.app.data.storage.CountryStorageRepository
import com.Globe.Titans.largest.population.app.data.storage.implementation.CountriesExternalStorage
import com.Globe.Titans.largest.population.app.domain.repository.CountryRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object GlobeTitansAppModule {
    @Provides
    @Singleton
    fun providesCountryStorage(): CountryStorageRepository{
        return CountriesExternalStorage()
    }
    @Provides
    @Singleton
    fun providesCountryRepository(storageRepository: CountryStorageRepository): CountryRepository {
        return CountryRepositoryImpl(storageRepository)
    }
}