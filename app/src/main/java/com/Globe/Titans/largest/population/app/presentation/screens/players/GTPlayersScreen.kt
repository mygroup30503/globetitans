package com.Globe.Titans.largest.population.app.presentation.screens.players

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.Globe.Titans.largest.population.app.R
import com.Globe.Titans.largest.population.app.data.country.CountryModel
import com.Globe.Titans.largest.population.app.presentation.utils.Fonts
import com.basketball.modern.app.era.presentation.theme.GTMain
import com.mundojogo.info.app.holdem.presentation.screens.info.GTPlayersScreenViewModel

@Composable
fun GTPlayersScreen(navHostController: NavHostController) {
    val vm: GTPlayersScreenViewModel = hiltViewModel()
    val players = vm.t.collectAsState()
    CountryItemUI(data = players.value, navHostController = navHostController, {
        vm.getNext()
    }, {
        vm.getLast()
    })


}

@Composable
fun CountryItemUI(
    data: CountryModel,
    navHostController: NavHostController,
    onNext: () -> Unit,
    onLast: () -> Unit
) {
    val scrolling = rememberScrollState()
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Black.copy(alpha = 0.6f))
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
        ) {
            Image(painter = painterResource(id = R.drawable.icon_back), contentDescription = null,
                modifier = Modifier
                    .clickable {
                        onLast.invoke()
                    }
                    .weight(1f)
                    .padding(8.dp)
            )
            Text(
                text = data.title, modifier = Modifier
                    .weight(5f)
                    .fillMaxHeight(), style = TextStyle(
                    color = Color.White,
                    fontFamily = Fonts.font,
                    textAlign = TextAlign.Center,
                    fontSize = 22.sp
                )
            )
            Box(modifier = Modifier.weight(1f))
        }
        Column(
            modifier = Modifier
                .weight(7f)
                .verticalScroll(scrolling)

        ) {
            Image(
                painter = painterResource(id = data.countryImage),
                contentDescription = null,
                modifier = Modifier.fillMaxWidth(),
                contentScale = ContentScale.FillWidth
            )
            Text(
                text = data.description, modifier = Modifier.padding(16.dp),
                fontFamily = Fonts.font,
                style = TextStyle(
                    fontSize = 16.sp, color = Color.White, fontFamily = Fonts.font
                )
            )
        }
        Box(
            modifier = Modifier
                .padding(vertical = 4.dp, horizontal = 32.dp)
                .fillMaxWidth()
                .weight(1f),
            contentAlignment = Alignment.Center
        ) {
            Button(
                onClick = {
                    if (data.title.startsWith("Vietnam")) navHostController.popBackStack()
                    else onNext.invoke()
                },
                colors = ButtonDefaults.buttonColors(
                    containerColor = GTMain
                )
            ) {
                Text(
                    text = "next",
                    fontFamily = Fonts.font,
                    color = Color.Black
                )
            }
        }
    }
}


