package com.Globe.Titans.largest.population.app.data.repository

import com.Globe.Titans.largest.population.app.data.country.CountryModel
import com.Globe.Titans.largest.population.app.data.storage.CountryStorageRepository
import com.Globe.Titans.largest.population.app.domain.repository.CountryRepository
import javax.inject.Inject

class CountryRepositoryImpl @Inject constructor(private val countryStorageRepository: CountryStorageRepository) :
    CountryRepository {
    override fun getFirstCountry(): CountryModel {
        repeat(15) {
            countryStorageRepository.iteratorMinus()
        }
        return countryStorageRepository.getCountry()
    }

    override fun getNextCountry(): CountryModel {
        countryStorageRepository.iteratorPlus()
        return countryStorageRepository.getCountry()
    }

    override fun getLastCountry(): CountryModel {
        countryStorageRepository.iteratorMinus()
        return countryStorageRepository.getCountry()
    }
}