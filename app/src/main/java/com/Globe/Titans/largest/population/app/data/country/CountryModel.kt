package com.Globe.Titans.largest.population.app.data.country

data class CountryModel(
    val title: String,
    val countryImage: Int,
    val description: String
)
