package com.Globe.Titans.largest.population.app.presentation.screens.home

import android.app.Activity
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.Globe.Titans.largest.population.app.presentation.utils.GlobeTitansScreens
import com.basketball.modern.app.era.presentation.theme.GTMain

@Composable
fun GTHomeScreen(navHostController: NavHostController) {
    val mainActivity = (LocalContext.current as? Activity)
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Black.copy(alpha = 0.6f)),
        contentAlignment = Alignment.Center
    ) {
        Row(modifier = Modifier.fillMaxWidth()) {
            Box(modifier = Modifier.weight(1f))
            Column(modifier = Modifier.weight(3f)) {
                Button(
                    onClick = { navHostController.navigate(GlobeTitansScreens.GT_COUNTRIES) },
                    colors = ButtonDefaults.buttonColors(
                        containerColor = GTMain
                    ), modifier = Modifier.fillMaxWidth()
                ) {
                    Text(text = "largest population", color = Color.Black)
                }
                Spacer(modifier = Modifier.height(40.dp))
                Button(
                    onClick = { mainActivity?.finish() },
                    colors = ButtonDefaults.buttonColors(
                        containerColor = GTMain
                    ), modifier = Modifier.fillMaxWidth()
                ) {
                    Text(text = "exit", color = Color.Black)
                }
            }
            Box(modifier = Modifier.weight(1f))
        }
    }
}