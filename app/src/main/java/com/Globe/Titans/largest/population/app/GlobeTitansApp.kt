package com.Globe.Titans.largest.population.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class GlobeTitansApp: Application()