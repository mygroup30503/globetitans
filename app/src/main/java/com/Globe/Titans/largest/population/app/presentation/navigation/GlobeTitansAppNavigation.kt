package com.Globe.Titans.largest.population.app.presentation.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.Globe.Titans.largest.population.app.presentation.screens.home.GTHomeScreen
import com.Globe.Titans.largest.population.app.presentation.screens.players.GTPlayersScreen
import com.Globe.Titans.largest.population.app.presentation.utils.GlobeTitansScreens
import com.Globe.Titans.largest.population.app.presentation.screens.waiting.GTLoadingScreen

@Composable
fun GlobeTitansAppNavigation() {
    val navHostController = rememberNavController()
    NavHost(
        navController = navHostController,
        startDestination = GlobeTitansScreens.GT_LOADING
    ) {
        composable(GlobeTitansScreens.GT_LOADING){
            GTLoadingScreen(navHostController = navHostController)
        }
        composable(GlobeTitansScreens.GT_COUNTRIES){
            GTPlayersScreen(navHostController = navHostController)
        }
        composable(GlobeTitansScreens.GT_HOME){
            GTHomeScreen(navHostController = navHostController)
        }
    }
}